# Kivy environment installation
This project is for install kivy environment on Linux OS.

It incorporates  :

- General Linux system packages : setuptools, build-essential, python-dev...
- Python tools for developpers
- Versionning tools : Git & Mercurial
- Tools for Kivy : Pygame, Cython

Afetr installation, your project environment is into special home directory (/opt/your_project/erp_dev/your_repo)

This script has been tested on Linux mint 17 LTS and therefore still in beta.

