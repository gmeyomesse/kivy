# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

import os
import logging
from kivy.lang import Builder

_logger = logging.getLogger(__name__)


class ViewLoader(object):

    def __init__(self):
        _logger.info('Instanciate %s', ViewLoader.__name__)
        self.pid = os.getpid()

    def load_views(self):
        """
        Load all views in views folder
        """
        view_names = self._get_views()
        if view_names:
            for view_name in view_names:
                # load views files
                Builder.load_file('%s/../views/%s' % (os.path.dirname(__file__),view_name))

    def _get_views(self):
        """
        Get list of all view files
        """
        views_directory = os.path.dirname(os.path.abspath(__file__)) + '/../views'
        views_file_names = sorted(os.listdir(views_directory))
        return views_file_names
