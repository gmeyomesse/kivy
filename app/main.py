# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

""" core library."""

import models
import view_loader

# set server timezone in UTC before time module imported
__import__('os').environ['TZ'] = 'UTC'


if __name__ == '__main__':
    view_loader.view_loader.ViewLoader().load_views()
    models.pong_game.PongApp().run()
