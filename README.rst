App Module Repository
========================

This simple project is an example repo for Python projects.

`Learn more <https://bitbucket.org/gmeyomesse/kivy>`_.

---------------

If you want to learn more about ``kivy`` files, check out `this repository <https://kivy.org>`_.

✨🍰✨
