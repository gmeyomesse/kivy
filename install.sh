#!/bin/bash
set -e
set -u

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

echo
echo "
#########      KIVY INSTALL    ######## #########
#						#
#                 (__)				#
#                 (oo)				#
#           /------\/				#
#          / |    ||				#
#         *  /\---/\				#
#            ~~   ~~ 				#
#						#
########## ######### ######### ######### #######"

echo
echo
echo "This program is to set Kivy environmeni"

# Var
# Command run to your user profile
readonly USER_HOME=$(eval echo ~${SUDO_USER})
touch $USER_HOME/cmd
readonly USER=`whoami`

# Update os
sudo apt-get update

# Install necessary system packages
sudo apt-get install -y build-essential python2.7 python-setuptools python-dev python-gst0.10 ffmpeg gstreamer0.10-plugins-good libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsmpeg-dev libsdl1.2-dev libportmidi-dev libswscale-dev libavformat-dev libavcodec-dev libgl1-mesa-dev libgles2-mesa-dev zlib1g-dev

# Install versionning tools
sudo apt-get install -y git mercurial

# Install python envwrapper
sudo apt-get install -y python-pip
sudo pip install --upgrade pip
sudo pip install virtualenvwrapper

# Set bash conf
configure_bash_profile(){
	# Set location where virtualenv will live 
	# first change your shell setup (.bashrc .profile)
	echo "" >> $USER_HOME/.bashrc
	
	echo "# Python virtualenv" >> $USER_HOME/.bashrc
	
	echo "export WORKON_HOME=$HOME/.virtualenvs" >> $USER_HOME/.bashrc

	echo "mkdir -p $HOME/pyprojects" >> $USER_HOME/.bashrc

	echo "export PROJECT_HOME=$HOME/pyprojects" >> $USER_HOME/.bashrc
	
	echo "source /usr/local/bin/virtualenvwrapper.sh" >> $USER_HOME/.bashrc

	# reload startup .bashrc
	source $USER_HOME/.bashrc
}


# Set location where virtualenv will live 
# first change your shell setup (.bashrc .profile)
configure_bashrc_virtualenv(){
	# test if virtual env is configured
	if grep -q 'export WORKON_HOME*' $USER_HOME/.bashrc; then
		echo "found" > $USER_HOME/cmd
	else
		echo "notfound" > $USER_HOME/cmd
	fi

	cmd=$(head -n 1 $USER_HOME/cmd)
	if [ ! -z "$cmd" ];then
		if [ "$cmd" = "notfound" ];then
			cp $USER_HOME/.bashrc $USER_HOME/.bashrc_backup
			configure_bash_profile
		fi
	fi
}

# create virtualenv
while true; do
	read -p "Enter your python virtualenv name : " VIRTUALENV
	echo "$VIRTUALENV"
    	if [ ! -z $VIRTUALENV ];then
		configure_bashrc_virtualenv
		exit
	else
		echo "WARNING ! You must enter something..."
	fi
done

# Install current version of cython into the virtualenv
workon $VIRTUALENV && pip uninstall --yes cython
workon $VIRTUALENV && install -U cython

# Install other PyGame and Kivy dependencies
workon $VIRTUALENV && pip install -U numpy

# Install pygame into the virtualenv
workon $VIRTUALENV && hg clone https://bitbucket.org/pygame/pygame
workon $VIRTUALENV && cd pygame && python setup.py build
workon $VIRTUALENV && cd pygame && python setup.py install
workon $VIRTUALENV && rm -rf pygame

# Install stable version of Kivy into the virtualenv
workon $VIRTUALENV && pip install kivy

# Install development version of buildozer into the virtualenv
# git clone https://github.com/kivy/buildozer.git
# buildozer : is a tool to creating applicationpackages easily
# via pip (latest stable, recommended)
workon $VIRTUALENV && pip install buildozer

# Install a couple of dependencies for KivyCatalog
workon $VIRTUALENV && pip install --upgrade pygments docutil
